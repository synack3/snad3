# snad3

A Snort 3 docker container built on Alpine Linux.  This project is meant for exploratory
purposes, reading packet capture files from the command line.  No attempts have been
made to use this as a live or production IDS. 

# Building the Container

```
docker build --rm -t snad3 .
```

# How to Use

## Preparing Use

The following are suggested locations:

- `/usr/local/etc/snort/rules`: Place your rules files here. Alternately, mount an external
    rules directory to this location.
- `/pcap`: Place your packet capture to analyze here. Alternately, mount an external 
    pcap directory to this location.

## Launching the Container

```
docker run -it --name snad3 snad3:latest 
```

## Basic execution 

Assuming you have created a rules file at `/usr/local/etc/snort/rules/MY.rules` and a packet
    capture file at `/pcap/MY.pcap`:

```
snort -c /usr/local/etc/snort/snort.lua -R /rules/MY.rules -r /pcap/MY.pcap -A cmg
```

# References

This effort was made substantially easier because of work proceeding it.  The following referecnes
were extremely helpful in assembling this Dockerfile:

- [Community Package Installs on Alpine](https://stackoverflow.com/questions/64295467/how-to-install-hwloc-dev-package-on-docker-alpine-container)
- [John Lin's Docker Snort project](https://github.com/John-Lin/docker-snort/blob/master/Dockerfile)
- [Snort 3 on FreeBSD](https://snort-org-site.s3.amazonaws.com/production/document_files/files/000/000/176/original/Snort_3_on_FreeBSD_11.pdf)
- [Snort 3 on Ubuntu](https://snort-org-site.s3.amazonaws.com/production/document_files/files/000/003/979/original/Snort3_3.1.0.0_on_Ubuntu_18___20.pdf)
