FROM alpine:3.13

RUN echo 'https://dl-cdn.alpinelinux.org/alpine/edge/community' >> /etc/apk/repositories && \
  apk add --no-cache --update alpine-sdk libunwind-dev linux-headers libpcap-dev cmake libdnet-dev \
    hwloc-dev luajit-dev openssl-dev pcre-dev libtirpc-dev flatbuffers-dev hyperscan-dev flex flex-dev

ENV SRC_HOME=/snort_src
WORKDIR ${SRC_HOME}

ARG SAFEC_VER=02092020
ADD https://github.com/rurban/safeclib/releases/download/v${SAFEC_VER}/libsafec-${SAFEC_VER}.tar.gz ${SRC_HOME}/libsafec-${SAFEC_VER}.tar
RUN tar -xzvf libsafec-${SAFEC_VER}.tar && \
  cd libsafec-${SAFEC_VER}.0-g6d921f && \
  ./configure && \
  make && \
  make install

ARG GPERF_VER=2.9.1
ADD  https://github.com/gperftools/gperftools/releases/download/gperftools-${GPERF_VER}/gperftools-${GPERF_VER}.tar.gz ${SRC_HOME}/gperftools-${GPERF_VER}.tar
RUN cd ${SRC_HOME} && \
  tar -xvf gperftools-$GPERF_VER.tar && \
  cd ${SRC_HOME}/gperftools-$GPERF_VER && \
  ./configure && \
  make && \
  make install

ARG DAQ_VER=3.0.2
ADD https://github.com/snort3/libdaq/releases/download/v${DAQ_VER}/libdaq-${DAQ_VER}.tar.gz $SRC_HOME/libdaq-${DAQ_VER}.tar
RUN cd ${SRC_HOME} && \
  tar xvf libdaq-${DAQ_VER}.tar && \
  cd ${SRC_HOME}/libdaq-${DAQ_VER} && \
  ./configure && \
  make && \
  make install

ARG SNORT_VER=3.1.3.0
ADD https://github.com/snort3/snort3/archive/${SNORT_VER}.tar.gz ${SRC_HOME}/snort3-${SNORT_VER}.tar
RUN cd ${SRC_HOME} && \
  tar xvf snort3-${SNORT_VER}.tar && \
  cd ${SRC_HOME}/snort3-${SNORT_VER} && \
  ./configure_cmake.sh --prefix=/usr/local --enable-tcmalloc && \
  cd build && \
  make && \
  make install

RUN rm -Rf /${SRC_HOME} && \
  mkdir /usr/local/etc/snort/rules && \
  mkdir /pcap && \
  addgroup snort3 && \
  adduser -h /pcap -G snort3 -D snort3

USER snort3

WORKDIR /pcap
ENTRYPOINT ["/bin/sh"]